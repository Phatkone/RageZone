import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;
import org.ini4j.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
/**
 * Black User Deleter tool
 * Deletes files in the blackuser folder repeating on the timeout specified.
 *
 * @author Phatkone/EuphoriA
 * @version 1.0
 */
public class main
{
    /**
     * Constructor for objects of class main
     */
    public static void main(String args[])
    {
        Map<String, String> settings = settings();
        String timeout = settings.get("timeout");
        String folder = settings.get("folder");
        String logFile = settings.get("logFile");
        Boolean doLog = Boolean.parseBoolean(settings.get("doLog"));
        double time = Double.parseDouble(timeout);
        long timer = (long)(time * 60 * 1000);
        int count = 0;
        if (doLog)
        {
            System.out.println("Logging Enabled"+System.lineSeparator()+"Writing to "+logFile);
        }
        else
        {
            System.out.println("Not Logging... '"+doLog+"' log file location "+logFile);
        }
        while (true)
        {
            System.out.println("Deleting bur files");
            cleanOut(folder, doLog, logFile);
            try 
            {
                System.out.println("Sleeping for "+timer+" microseconds");
                Thread.sleep(timer);
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
            }
            count++;
        }
    }

    private static void cleanOut(String folder, boolean doLog, String logFile)
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String today = dateFormat.format(date);
        File dir = new File(folder);
        if (dir.exists())
        {
            File[] fList = dir.listFiles();
            for (File file : fList)
            {
                String ext = "";
                int i = file.getName().lastIndexOf('.');
                if (i > 0)
                {
                    ext = file.getName().substring(i+1);
                }
                if (ext.toLowerCase().equals("bur"))
                {
                    //if (file.delete())
                    System.out.println("Deleting File: "+file.getName());
                    
                    if (file.delete())
                    {
                        System.out.println("File: "+file.getName()+" Deleted");
                        if (doLog)
                        {
                            debugLog(today+" - "+file.getName()+" Deleted", logFile);
                        }
                    }
                    else
                    {
                        System.out.println("Error Deleting: "+file.getName());
                        if (doLog)
                        {
                            debugLog(today+" - "+file.getName()+" Failed To Delete", logFile);
                        }
                    }
                }
                //files.add(file);
            }
        }
    }
    
    private static void debugLog(String message, String logFile)
    {
        try 
        {
            FileWriter writer = new FileWriter(logFile, true);
            writer.write(message);
            writer.write(System.lineSeparator());
            writer.close();
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    
    private static Map<String,String> settings()
    {
        Map<String, String> settings = new HashMap<String, String>();
        boolean iniFound = false;
        Config.getGlobal().setEscape(false);
        String settingsFile = "settings.ini";
        try 
        {
            Ini ini = new Ini(new File(settingsFile));
            iniFound = true;
            settings.put("folder",ini.get("BUDeleter","folder"));
            settings.put("timeout",ini.get("BUDeleter","timeout"));
            settings.put("doLog",ini.get("BUDeleter","doLog"));
            settings.put("logFile",ini.get("BUDeleter","logFile"));
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        if (!iniFound)
        {
            settings.put("folder","C:\\PT-Server\\BlackUser");
            settings.put("timeout","5");
            settings.put("doLog","true");
            settings.put("logFile","C:\\PT-Server\\BlackUser\\BUDeleter.log");
            try 
            {
                FileWriter writer = new FileWriter(settingsFile);
                writer.write("[BUDeleter]");
                writer.write(System.lineSeparator());
                writer.write(";Blackuser Folder");
                writer.write(System.lineSeparator());
                writer.write("folder = C:\\PT-Server\\BlackUser");
                writer.write(System.lineSeparator());
                writer.write(";Sleep time between runs");
                writer.write(System.lineSeparator());
                writer.write("timeout = 5");
                writer.write(System.lineSeparator());
                writer.write(";Log activity (True or False)");
                writer.write(System.lineSeparator());
                writer.write("doLog = True");
                writer.write(System.lineSeparator());
                writer.write(";Log File");
                writer.write(System.lineSeparator());
                writer.write("logFile = C:\\PT-Server\\BlackUser\\BUDeleter.log");
                writer.write(System.lineSeparator());
                writer.close();
            }
            catch (IOException e)
            {
                System.out.println(e.getMessage());
            }
        }
        
        return settings;
    }
}
