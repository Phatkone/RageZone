<?php
/** 
*	Clan Kick User Script 
*	By EuphoriA
**/
ini_set('display_errors',1);
include 'settings.php';
if (!defined("RZAPT"))
	die();
/* Setting GET Values to Variables */
$userid = (isset($userid)) ? $userid : "";
$gserver = (isset($gserver)) ? $gserver : "";
$chname = (isset($chname)) ? $chname : "";
$ticket = (isset($ticket)) ? $ticket : "";
$clName = (isset($clName)) ? $clName : "";
$clwon1 = [];
if (isset($uri))
{
	foreach ($uri as $k => $v)
	{
		if (substr($k,0,5) === 'clwon')
		{
			// array_push($clwon1,cleanStr($v));
			$clwon1[] = cleanStr($v);
		}
	}
}
/* Stop script if one or more of the variables are not set. */
if ($userid == "" || $gserver == "" || (empty($clwon1) || $clwon1 == "") || $chname == "" || $ticket == "" || $clName == "")
{
	print("Code=100$CR");
	$dbconn = null;
	die();
}
if (is_array($clwon1))
{
	foreach ($clwon1 as $clwon)
	{
		clankick($clwon);
	}
}
else
{
	clankick($clwon1);
}
$dbconn = null;
/**		Kick method, will stop on errors for any of the members being kicked.	**/
function clankick($clwon1)
{
	global $dbconn, $userid, $gserver, $chname, $ticket, $clName, $CR;
	$query = "SELECT ClanName 
		FROM ClanDB.dbo.UL 
		WHERE ChName='$clwon1'";
	$stmt = $dbconn->prepare($query);
	$stmt->execute();
	$result = $stmt->fetch(PDO::FETCH_ASSOC);
	if (!empty($result))
	{
		$clName2 = $result['ClanName'];
		if ($clName2 == "")
		{
			$query = "DELETE 
						FROM ClanDB.dbo.UL 
						WHERE ChName='$clwon1'";
			$stmt = $dbconn->prepare($query);
			$stmt->execute();
			$dbconn = null;
			die("Code=01$CR");
		}
		if ($clName2 != $clName)
		{
			$dbconn = null;
			die("Code=02$CR");
		}
	}
	else
	{
		$dbconn = null;
		die("Code=0$CR");
	}
	$query = "SELECT ClanZang,MemCnt 
				FROM ClanDB.dbo.CL 
				WHERE ClanName='$clName'";
	$stmt = $dbconn->prepare($query);
	$stmt->execute();
	$results = $stmt->fetch(PDO::FETCH_ASSOC);
	$ClanLeader = $results['ClanZang'];
	$ClanMembers = $results['MemCnt'];
	$ClanMembers = $ClanMembers - 1;
	if ($clwon1 == $ClanLeader)
	{
		$dbconn = null;
		die("Code=4$CR");
	}
	$query = "UPDATE ClanDB.dbo.CL 
				SET MemCnt='$ClanMembers' 
				WHERE ClanName='$clName';
			DELETE 
				FROM ClanDB.dbo.UL 
				WHERE ChName='$clwon1';";
	$stmt = $dbconn->prepare($query);
	$stmt->execute();
	print("Code=1$CR");
}
?>