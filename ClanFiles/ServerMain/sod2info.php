<?php
include '..\\ServerMain\\settings.php';
if (!defined("RZAPT"))
	die();
$CR2 = chr(124);
$userid = (isset($userid)) ? $userid : "";
$gserver = (isset($gserver)) ? $gserver : "";
$chname = (isset($chname)) ? $chname : "";
$index = (isset($index)) ? $index : "";
if ($userid == "" || $gserver == "" || $chname == "")
{
	die("Code=100");
}
if ($index == "")
{
	die("Code=104");
}
if ($dbconn)
{
	switch ($index)
	{
		case "1": //Main Karina Page
			$query = "SELECT * 
						FROM ClanDB.dbo.CL 
						ORDER BY Cpoint DESC";
			$stmt = $dbconn->prepare($query);
			$stmt->execute();
			$result = $stmt->fetch();
			if (count($result) > 0)
			{
				$row = $result;
				$ClanPoint = $row['Cpoint'];
				$ClanNote = $row['Note'];
				$ClanName = $row['ClanName'];
				$ClanIMG = $row['MIconCnt'];
				$ClanLeader = $row['ClanZang'];
				$ClanMoney = $row['ClanMoney'];
				
				$query = "SELECT * 
							FROM ClanDB.dbo.UL 
							WHERE chname='$chname'";
				$stmt = $dbconn->prepare($query);
				$stmt->execute();
				$row = $stmt->fetchAll();
				if (!count($row) > 0)
				{
					$iNum = 0;
					$return = "Code=".$iNum.$CR2."CClanMoney=0 $CR 2CTax=0 $CR2 CName= $CR2 CNote= $CR2 CZang= $CR2 CIMG= $CR2";
				}
				else
				{				
					$ClanSub = $row['ClanName'];
					$ClanSubChief = $row['Permi'];
					if ($ClanName == $ClanSub)
					{
						if ($ClanLeader == $chname)
						{
							$iNum = 1;
							$return = "Code=$iNum".$CR2."CClanMoney=$ClanMoney".$CR2."CTax=0".$CR2."CName=$ClanName".$CR2."CNote=$ClanNote".$CR2."CZang=$ClanLeader".$CR2."CIMG=$ClanIMG".$CR2."TotalEMoney=$ClanMoney".$CR2."TotalMoney=$ClanMoney".$CR2;
						}
						elseif ($ClanSubChief = 2)
						{
							$iNum = 2;
							$return = "Code=$iNum".$CR2."CClanMoney=0".$CR2."CTax=0".$CR2."CName=$ClanName".$CR2."CNote=$ClanNote".$CR2."CZang=$ClanLeader".$CR2."CIMG=$ClanIMG".$CR2."TotalEMoney=$ClanMoney".$CR2."TotalMoney=$ClanMoney".$CR2;
						}
						else 
						{
							$iNum = 3;
							$return = "Code=$iNum".$CR2."CClanMoney=0".$CR2."CTax=0".$CR2."CName=$ClanName".$CR2."CNote=$ClanNote".$CR2."CZang=$ClanLeader".$CR2."CIMG=$ClanIMG".$CR2."TotalEMoney=$ClanMoney".$CR2."TotalMoney=$ClanMoney".$CR2;
						}
					}
					else
					{
						$query = "SELECT ClanZang 
									FROM Clandb.dbo.CL 
									WHERE ClanName='$ClanSub'";
						$stmt = $dbconn->prepare($query);
						$stmt->execute();
						$row = $stmt->fetchAll();
						$ClanSubLeader = $row['ClanZang'];
						if ($ClanSubLeader == $chname)
						{
							$iNum = 4;
							$return = "Code=$iNum".$CR2."CClanMoney=0".$CR2."CTax=0".$CR2."CName=".$ClanName.$CR2."CNote=".$ClanNote.$CR2."CZang=".$ClanLeader.$CR2."CIMG=$ClanIMG".$CR2;
						}
						elseif ($ClanSubChief == 2)
						{
							$iNum = 5;
							$return = "Code=$iNum".$CR2."CClanMoney=0".$CR2."CTax=0".$CR2."CName=".$ClanName.$CR2."CNote=".$ClanNote.$CR2."CZang=".$ClanLeader.$CR2."CIMG=$ClanIMG".$CR2;
						}
						else
						{
							$iNum = 6;
							$return = "Code=$iNum".$CR2."CClanMoney=0".$CR2."CTax=0".$CR2."CName=".$ClanName.$CR2."CNote=".$ClanNote.$CR2."CZang=".$ClanLeader.$CR2."CIMG=$ClanIMG".$CR2;
						}
					}
				}
			}
			else
			{
				$return = "Code=0";
			}
			break;
		Case "3": //High Score Clan List
			$query = "SELECT TOP(10) * 
						FROM ClanDB.dbo.CL 
						ORDER BY Cpoint DESC";
			$stmt = $dbconn->prepare($query);
			$stmt->execute();
			$result = $stmt->fetchAll();
			if (!count($result) > 0)
			{
				$return = "Code=0";
			}
			else
			{
				$return = "Code=1$CR";
				$i = 0;
				$rescount = count($result);
				while ($i < $rescount)
				{
					if ($i >= 9)
					{
						$i = count($result);
					}
					$tSubRegiDate = "";
					$tSub2 = "";
					$tSub3 = 0;
					$row = $result[$i];
					$ClanName = $row['ClanName'];
					$tSub = $row['RegiDate']->format('n/j/Y');
					$x = 0;
					while ($x <= strlen($tSub))
					//for ($x = 1; $x <= strlen($tSub); $x++)
					{
						if ($tSub3 == 0)
						{
							$tSub2 = substr($tSub, $x, 1);
							if ($tSub2 != " ")
							{
								$tSubRegiDate .= $tSub2;
							}
							else
							{
								$tSub3 = 1;
							}
						}
						$x++;
					}
					if (!stripos($return, $ClanName) !== false && $row['Cpoint'] > 0)
					{
						$return = $return."CIMG=".$row['MIconCnt'].$CR."CName=$ClanName $CR CPoint=".$row['Cpoint'].$CR."CRegistDay=$tSubRegiDate $CR";
					}
					$i++;
				}
			}
			break;
		default: 
			$return = "Code=104";
	}
	$dbconn = null;
}
else
{
	$return = "Code=100";
}
echo $return;
?>