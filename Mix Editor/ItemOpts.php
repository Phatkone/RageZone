<?php
echo "<option value=''>N/A</option>
<optgroup label='Weapons'>
<option value='axe'>Axe</option>
<option value='bow'>Bow</option>
<option value='claw'>Claw</option>
<option value='dagger'>Dagger</option>
<option value='javelin'>Javelin</option>
<option value='mace'>Mace</option>
<option value='phantom'>Phantom</option>
<option value='scythe'>Scythe</option>
<option value='sword'>Sword</option>
<option value='vambrance'>Vambrance</option>
<option value='wand'>Wand</option>
</optgroup>
<optgroup label='Defence'>
<option value='amulet'>Amulet</option>
<option value='armor'>Armor</option>
<option value='armlet'>Armlet</option>
<option value='boots'>Boots</option>
<option value='gloves'>Gloves</option>
<option value='orb'>Orb</option>
<option value='ring'>Ring</option>
<option value='robe'>Robe</option>
<option value='shield'>Shield</option>
</optgroup>";
?>