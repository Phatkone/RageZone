<?php
/**
*    Server Mix Reader and Editor
*	 Written By Phatkone/EuphoriA
**/
$server = "C:\\jpt-server\\server4096.exe";//"C:\PT-Server\Server1024.exe"; //ePT //File to be read.
$size = 0x1a4; //0x1AC; //ePT // Length of each mix.
$start = 0x2bb6a8; //0x40DBB0; //ePT //Offset of first mix, starts with 00 00 01 01 for axe (default 5 lucidy mix) 
$length = $size * 300; //0x1f590; //Total length of mix section (standard is 300x the mix size)
$writefile = 'C:\PT-Server\Server1024.exe'; //Output file, change if you don't want to overwrite your existing.
?>
<!--/**
*    Server Mix Reader and Editor
*	 Written By Phatkone/EuphoriA
**/-->
<style>
	body {
		background-color: beige;
		text-align: center;
		margin: 0 auto;
	}
	div {
		border: 2px black solid;
		border-radius: 10px;
		margin: 0 auto;
		margin-top: 15px;
		margin-bottom: 15px;
		background-color: grey;
		width: 80vw;
	}
	h3 {
		margin-top: 15px;
		margin-bottom: 15px;
	}
	.header {
		border-radius: 0px 0px 5px 5px;
		position: fixed;
		top: 0;
		left: 0;
		width: 99vw;
		height: 75px;
		margin:0 auto;
		text-align: center;
		z-index: 2;
	}
	#jump {
		position: fixed;
		top: 0px;
		left: 0px;
		z-index: 5;
		width: 200px;
		border: none;
		background: none;
	}
	.footer {
		border-radius: 5px 5px 0px 0px;
		position: fixed;
		bottom: 0;
		left:0;
		z-index: 2;
		width: 99vw;
		height: 25px;
		margin: 0 auto;
		padding-top: 2px;
		padding-bottom: 2px;
		background-color: #222;
		color: #999;
	}
	table {
		width: 70vw;
		margin: 0 auto;
		background-color: lightgrey;
		color: #333;
		font-weight: bold;
		border-radius: 5px;
	}
	td {
		border: 1px solid black;
		text-align: center;
		margin: 0 auto;
	}
	td:nth-child(odd) {
		width: 15vw;
	}
	input {
		text-align: center;
	}
	input[name=MixNo] {
		width:100%;
	}
	.item {
		width: 100%;
	}
	.sheltom {
		position: relative;
		right: 0px;
		float: right;
		width: 75%
	}
	.effect {
		width: 65%;
		float:left;
		height:100%
	}
	.effectval {
		width: 25%;
	}
	.submit {
		width: 45%;
		margin: 0 auto;
		margin-top: 5px;
		margin-bottom: 5px;
		float: left;
		margin-left: 20px;
		font-weight: bold;
		background-color: grey;
		border: 2px solid black;
		border-radius: 5px;
	}
	.clear {
		width: 45%;
		margin: 0 auto;
		margin-top: 5px;
		margin-bottom: 5px;
		float: right;
		margin-right: 20px;
		font-weight: bold;
		background-color: grey;
		border: 2px solid black;
		border-radius: 5px;
	}
	#mixstring {
		background-color: white;
		border: 1px solid white;
	}
</style>
<script src="jquery-2-1-1.min.js">
//https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"</script>
<script type='text/javascript'>
	$(document).ready(function () {
		$('#jumpGo').click(function(e) {
			e.preventDefault;
			var yLoc = goTo($('#jumpVal').val());
			window.scroll({
			 top: 0,
			 left: yLoc - 150,
			 behavior: 'smooth'});
			
			//0,(yLoc - 150), 'smooth');
			if (yLoc == 0) {
				alert("Mix Does Not Exist");
			}
		});
	});
	function ClearThis(count) {
		var myform = document.getElementById(count);
		var Des = $(myform).find('input[name="MixDes"]').val();
		var conf = confirm('Are you sure you wish to delete the following mix? \n '+Des);
		if (!conf) {
			return;
		}
		else {
			$(myform).find('input:checkbox').each( function() {
				$(this).removeAttr('checked');
			});
			$(myform).find('select').each( function() {
				$(this).val('');
			});
			$(myform).find('input:text').each( function() {
				if ($(this).attr('name') != 'MixNo' && $(this).attr('name') != 'mode') {
					var sheltoms = ['lucidy', 'sereneo', 'sparky', 'fadeo', 'transparo', 'raident', 'murky', 'devine', 'celesto'];
					var inputname = $(this).attr('name');
					if (inputname.substring(0,11) == 'effectvalue') {
						$(this).val('0');
					}
					if (inArray(inputname, sheltoms) != false) {
						$(this).val('0');
					}
					if ($(this).attr('name') == 'MixDes') {
						$(this).val('');
					}
					
				}
			});
			setTimeout(function() {myform.submit();}, 100);
		}
	}
	function inArray(needle,haystack) {
		for (k in haystack) {
			if (haystack[k] == needle) {
				return true;
			}
		}
		return false;
	}
	function goTo(id) {
		var ID = document.getElementById(id);
		if (ID == undefined) {
			return 0;
		}
		var spot = $(ID).position();
		return spot.top;
	}
</script>
<?php
/**  
*
*	Reads the Mixes from the server and puts into readable tables 
*		Edits the mix as specified in mixcreate.php or as edited in this.
*
**/
$goto = (isset($_GET['goto'])) ? $_GET['goto'] : null;
if ($goto != null) 
{
	/*  JQuery to make the page jump to the mix you want to see.  */
	echo "<script type='text/javascript'> 
		$(document).ready( function() {
			var yLoc = goTo({$goto});
			window.scroll(0,(yLoc - 150));
		});
	</script>";
}
$mode = (isset($_POST['mode'])) ? $_POST['mode'] : "read";
require "functions.php";
/*  Handling  */
if ($mode == "read")
{
	if (is_file($server))
	{
		$fOpen = fopen($server,'r');
		$fRead = fread($fOpen, filesize($server));
		$mixes = substr($fRead, $start, $length);
		$split = str_split($mixes, $size);
		$count = 0;
		$activemix = 0;
		$lastmix = 0;
		echo "<div class='header'><h3> Mix Viewer / Editor By Phatkone/EuphoriA</h3><b>Note:</b> Put a pipe '<b>|</b>' in the description to enter a new line.</div><br /><br /><br /><br />
		<div id='jump'>Jump To Mix:<br /><input type='text' min='1' id='jumpVal' style='width: 50px;' /><button type='button' id='jumpGo' >Go!</button><br /></div>
		<div class='footer'>Loading Mixes...</div>";
		
		/*  Handling of each mix as the section of code.  */
		foreach ($split as $mix) {
			/*  Extracting the sections of the mixes and parsing through the handler functions to make them readable  */
			$count++;
			$i = 0x02;
			$item1 = items(substr($mix,$i,2)); //0x02,2));
			 $i = $i + 4;
			$item2 = items(substr($mix,$i,2)); //0x06,2));
			 $i = $i + 4;
			$item3 = items(substr($mix,$i,2)); //0x0a,2));
			 $i = $i + 4;
			$item4 = items(substr($mix,$i,2)); //0x0e,2));
			 $i = $i + 4;
			$item5 = items(substr($mix,$i,2)); //0x12,2));
			 $i = $i + 4;
			$item6 = items(substr($mix,$i,2)); //0x16,2));
			 $i = $i + 4;
			$item7 = items(substr($mix,$i,2)); //0x1a,2));
			 $i = $i + 4;
			$item8 = items(substr($mix,$i,2)); //0x1e,2));
			 $i = $i + 4;
			$item9 = items(substr($mix,$i,2)); //0x22,2));
			 $i = $i + 4;
			$item10 = items(substr($mix,$i,2)); //0x26,2));
			 $i = $i + 2;
			$lucidy = ord(substr($mix,$i,1)); //0x28,1));
			 $i = $i + 4;
			$sereneo = ord(substr($mix,$i,1)); //0x2c,1));
			 $i = $i + 4;
			$fadeo = ord(substr($mix,$i,1)); //0x30,1));
			 $i = $i + 4;
			$sparky = ord(substr($mix,$i,1)); //0x34,1));
			 $i = $i + 4;
			$raident = ord(substr($mix,$i,1)); //0x38,1));
			 $i = $i + 4;
			$transparo = ord(substr($mix,$i,1)); //0x3c,1));
			 $i = $i + 4;
			$murky = ord(substr($mix,$i,1)); //0x40,1));
			 $i = $i + 4;
			$devine = ord(substr($mix,$i,1)); //0x44,1));
			 $i = $i + 4;
			$celesto = ord(substr($mix,$i,1)); //0x48,1));
			 $i = $i + 4;
			$effect1 = effect(substr($mix,$i,4)); //0x4c,4));
			 $i = $i + 4;
			$effect2 = effect(substr($mix,$i,4)); //0x50,4));
			 $i = $i + 4;
			$effect3 = effect(substr($mix,$i,4)); //0x54,4));
			 $i = $i + 4;
			$effect4 = effect(substr($mix,$i,4)); //0x58,4));
			 $i = $i + 4;
			$effect5 = effect(substr($mix,$i,4)); //0x5c,4));
			 $i = $i + 4;
			$effect6 = effect(substr($mix,$i,4)); //0x60,4));
			 $i = $i + 4;
			$effect7 = effect(substr($mix,$i,4)); //0x64,4));
			 $i = $i + 4;
			$effect8 = effect(substr($mix,$i,4)); //0x68,4));
			 $i = $i + 4;
			$value1 = value(substr($mix,$i,4)); //0x6c,4));
			 $i = $i + 4;
			$value2 = value(substr($mix,$i,4)); //0x70,4));
			 $i = $i + 4;
			$value3 = value(substr($mix,$i,4)); //0x74,4));
			 $i = $i + 4;
			$value4 = value(substr($mix,$i,4)); //0x78,4));
			 $i = $i + 4;
			$value5 = value(substr($mix,$i,4)); //0x7c,4));
			 $i = $i + 4;
			$value6 = value(substr($mix,$i,4)); //0x80,4));
			 $i = $i + 4;
			$value7 = value(substr($mix,$i,4)); //0x84,4));
			 $i = $i + 4;
			$value8 = value(substr($mix,$i,4)); //0x88,4));
			 $i = $i + 4;
			$pc1 = percent(substr($mix,$i,1)); //0x8c,1));
			 $i = $i + 4;
			$pc2 = percent(substr($mix,$i,1)); //0x90,1));
			 $i = $i + 4;
			$pc3 = percent(substr($mix,$i,1)); //0x94,1));
			 $i = $i + 4;
			$pc4 = percent(substr($mix,$i,1)); //0x98,1));
			 $i = $i + 4;
			$pc5 = percent(substr($mix,$i,1)); //0x9c,1));
			 $i = $i + 4;
			$pc6 = percent(substr($mix,$i,1)); //0xa0,1));
			 $i = $i + 4;
			$pc7 = percent(substr($mix,$i,1)); //0xa4,1));
			 $i = $i + 4;
			$pc8 = percent(substr($mix,$i,1)); //0xa8,1));
			 $i = $i + 4;
			$des = substr($mix,$i,256); //0xac,256);
			
			/*  Removes the "." from the description which causes issues when re-writing mixes. although hex value 00, comes through as hex value 46  */
			$dessplit = str_split($des, 1);
			$des = "";
			foreach ($dessplit as $chr)
			{
				if (ord($chr) != "" || ord($chr) != chr(46))
				{
					if (ord($chr) == 0x0d) 
					{
						$des .= "|";
					}
					else
					{
						$des .= $chr;
					}
				}
			}
			$des = trim($des, '|');
			$items = array($item1, $item2, $item3, $item4, $item5, $item6, $item7, $item8, $item9, $item10);
			$itemcount = 0;
			foreach  ($items as $item)
			{
				if ($item != "")
				{
					$itemcount++;
				}
			}
			if ($itemcount > 0)
			{
				/*  Displays existing mixes in a form so that they can be easily edited  */
				echo "<div><h4>Mix Number: $count</h4><form method='POST' id='$count' action='".htmlspecialchars($_SERVER['PHP_SELF'])."'><input style='display:none;' name='mode' value='edit' />";
				echo "<table id='Mix$count'>";
				echo  "<tr><td><input name='MixNo' style='display:none;' value='$count' readonly='readonly' />Description:</td><td><input name='MixDes' value ='$des' style='width:100%' required /></td></tr>";
				echo  "<tr><td rowspan='10'>Items: </td><td><select class='item' name='item1' required ><option value='".strtolower($item1)."'> $item1 </option>";
				include 'itemopts.php';
				echo "</select></td></tr>";
				echo "<tr><td><select class='item' name='item2'><option value='".strtolower($item2)."'> $item2 </option>";
				include 'itemopts.php';
				echo "</select></td></tr>";
				echo "<tr><td><select class='item'  name='item3'><option value='".strtolower($item3)."'> $item3 </option>";
				include 'itemopts.php';
				echo "</select></td></tr>";
				echo "<tr><td><select class='item'  name='item4'><option value='".strtolower($item4)."'> $item4 </option>";
				include 'itemopts.php';
				echo "</select></td></tr>";
				echo "<tr><td><select class='item'  name='item5'><option value='".strtolower($item5)."'> $item5 </option>";
				include 'itemopts.php';
				echo "</select></td></tr>";
				echo "<tr><td><select class='item'  name='item6'><option value='".strtolower($item6)."'> $item6 </option>";
				include 'itemopts.php';
				echo "</select></td></tr>";
				echo "<tr><td><select class='item'  name='item7'><option value='".strtolower($item7)."'> $item7 </option>";
				include 'itemopts.php';
				echo "</select></td></tr>";
				echo "<tr><td><select class='item'  name='item8'><option value='".strtolower($item8)."'> $item8 </option>";
				include 'itemopts.php';
				echo "</select></td></tr>";
				echo "<tr><td><select class='item'  name='item9'><option value='".strtolower($item9)."'> $item9 </option>";
				include 'itemopts.php';
				echo "</select></td></tr>";
				echo "<tr><td><select class='item'  name='item10'><option value='".strtolower($item10)."'> $item10 </option>";
				include 'itemopts.php';
				echo "</select></td></tr>";
				echo "<tr><td rowspan='9'>Sheltoms: </td><td>Lucidy:<input name='lucidy' class='sheltom' value='$lucidy' /> </td></tr><tr><td> Sereneo:<input name='sereneo' class='sheltom' value='$sereneo' /></td></tr><tr><td> Fadeo:<input name='fadeo' class='sheltom' value='$fadeo' /></td></tr><tr><td> Sparky:<input name='sparky' class='sheltom' value='$sparky' /></td></tr><tr><td>	Raident:<input class='sheltom' name='raident' value='$raident' /></td></tr><tr><td> Transparo:<input class='sheltom' name='transparo' value='$transparo' /></td></tr><tr><td> Murky:<input name='murky' class='sheltom' value='$murky' /></td></tr><tr><td> Devine:<input name='devine' class='sheltom' value='$devine' /></td></tr><tr><td> Celesto:<input name='celesto' class='sheltom' value='$celesto' /> </td></tr>";
				echo "<tr><td rowspan='8'>Effects: </td><td><select class='effect' name='effect1' required > <option value='".effectarr($effect1)."' selected>$effect1 </option>";
				include 'effectopts.php';
				echo "</select><input class='effectval' name='effectvalue1' value='$value1' /><input type='checkbox' class='effectpc'  name='effectpercent1' ";
				echo ($pc1 == '%') ? "checked />%" : " />%";
				echo " </td></tr>";
				echo "<tr><td><select class='effect'  name='effect2'><option value='".effectarr($effect2)."' selected>$effect2 </option>";
				include 'effectopts.php';
				echo "</select><input class='effectval'  name='effectvalue2' value='$value2' /><input type='checkbox' class='effectpc'  name='effectpercent2' ";
				echo ($pc2 == '%') ? "checked />%" : " />%";
				echo " </td></tr>";
				echo "<tr><td><select class='effect'  name='effect3'> <option value='".effectarr($effect3)."' selected>$effect3 </option>";
				include 'effectopts.php';
				echo "</select><input class='effectval'  name='effectvalue3' value='$value3' /><input type='checkbox' class='effectpc'  name='effectpercent3' ";
				echo ($pc3 == '%') ? "checked />%" : " />%";
				echo " </td></tr>";
				echo "<tr><td><select class='effect'  name='effect4'> <option value='".effectarr($effect4)."' selected>$effect4 </option>";
				include 'effectopts.php';
				echo "</select><input class='effectval'  name='effectvalue4' value='$value4' /><input type='checkbox' class='effectpc'  name='effectpercent4' ";
				echo ($pc4 == '%') ? "checked />%" : " />%";
				echo " </td></tr>";
				echo "<tr><td><select class='effect'  name='effect5'> <option value='".effectarr($effect5)."' selected>$effect5 </option>";
				include 'effectopts.php';
				echo "</select><input class='effectval'  name='effectvalue5' value='$value5' /><input type='checkbox' class='effectpc'  name='effectpercent5' ";
				echo ($pc5 == '%') ? "checked />%" : " />%";
				echo " </td></tr>";
				echo "<tr><td><select class='effect'  name='effect6'> <option value='".effectarr($effect6)."' selected>$effect6 </option>";
				include 'effectopts.php';
				echo "</select><input class='effectval'  name='effectvalue6' value='$value6' /><input type='checkbox' class='effectpc'  name='effectpercent6' ";
				echo ($pc6 == '%') ? "checked />%" : " />%";
				echo " </td></tr>";
				echo "<tr><td><select class='effect'  name='effect7'> <option value='".effectarr($effect7)."' selected>$effect7 </option>";
				include 'effectopts.php';
				echo "</select><input class='effectval'  name='effectvalue7' value='$value7' /><input type='checkbox' class='effectpc'  name='effectpercent7' ";
				echo ($pc7 == '%') ? "checked />%" : " />%";
				echo " </td></tr>";
				echo "<tr><td><select class='effect'  name='effect8'> <option value='".effectarr($effect8)."' selected>$effect7 </option>";
				include 'effectopts.php';
				echo "</select><input class='effectval'  name='effectvalue8' value='$value8' /><input type='checkbox' class='effectpc'  name='effectpercent8' ";
				echo ($pc8 == '%') ? "checked />%" : " />%";
				echo " </td></tr>";
				echo "<tr><td colspan='2'><input type='submit' value='Submit'class='submit' />&nbsp&nbsp<button type='button' class='clear' onClick='ClearThis($count);' >Delete Mix</button></td></tr></table>
				</form></div>";
				$lastmix = $count + 1;
				$activemix++;
			}
		}
		/*  New mix form for adding mixes not already exists (up to the 300 mix limit)  */
		echo "<div><br /><form action='".htmlspecialchars($_SERVER['PHP_SELF'])."' method='POST'><input style='display:none;' name='mode' value='edit' /><table><tr><td colspan='2' style='text-align:center'><b>New Mix:</b></td></tr><tr><td>Mix Number</td><td><input type='text' name='MixNo' value='$lastmix' required /></td></tr><tr><td>Description: </td><td><input type='text' name='MixDes' style='width:100%'required  /></td></tr><tr><td>Item 1:</td><td><select class='item'  name='item1'required >";
		include 'itemopts.php';
		echo "</select></td></tr>
		<tr><td>Item 2:</td><td><select class='item'  name='item2'>";
		include 'itemopts.php';
		echo "</select></td></tr>
		<tr><td>Item 3:</td><td><select class='item'  name='item3'>";
		include 'itemopts.php';
		echo "</select></td></tr>
		<tr><td>Item 4:</td><td><select class='item'  name='item4'>";
		include 'itemopts.php';
		echo "</select></td></tr>
		<tr><td>Item 5:</td><td><select class='item'  name='item5'>";
		include 'itemopts.php';
		echo "</select></td></tr>
		<tr><td>Item 6:</td><td><select class='item'  name='item6'>";
		include 'itemopts.php';
		echo "</select></td></tr>
		<tr><td>Item 7:</td><td><select class='item'  name='item7'>";
		include 'itemopts.php';
		echo "</select></td></tr>
		<tr><td>Item 8:</td><td><select class='item'  name='item8'>";
		include 'itemopts.php';
		echo "</select></td></tr>
		<tr><td>Item 9:</td><td><select class='item'  name='item9'>";
		include 'itemopts.php';
		echo "</select></td></tr>
		<tr><td>Item 10:</td><td><select class='item'  name='item10'>";
		include 'itemopts.php';
		echo "</select></td></tr>";
		echo "<tr><td> Lucidy </td><td><input type='text' max='12' value='0' name='lucidy' style='width:100%;' /></td></tr>";
		echo "<tr><td> Sereneo </td><td><input type='text' max='12'  value='0' name='sereneo' style='width:100%;' /></td></tr>";
		echo "<tr><td> Fadeo </td><td><input type='text' max='12' value='0' name='fadeo' style='width:100%;' /></td></tr>";
		echo "<tr><td> Sparky </td><td><input type='text' max='12' value='0' name='sparky' style='width:100%;' /></td></tr>";
		echo "<tr><td> Raident </td><td><input type='text' max='12'  value='0' name='raident' style='width:100%;' /></td></tr>";
		echo "<tr><td> Transparo </td><td><input type='text' max='12'  value='0' name='transparo' style='width:100%;' /></td></tr>";
		echo "<tr><td> Murky </td><td><input type='text' max='12' value='0' name='murky' style='width:100%;' /></td></tr>";
		echo "<tr><td> Devine </td><td><input type='text' max='12'  value='0' name='devine' style='width:100%;' /></td></tr>";
		echo "<tr><td> Celesto </td><td><input type='text' max='12'  value='0' name='celesto' style='width:100%;' /></td></tr>";
		echo "<tr><td colspan='2'>Effects</td></tr>";
		echo "<tr><td><select name='effect1' style='width:100%;'>";
		include 'effectopts.php';
		echo "</select></td><td><input type='text' value='0'  name='effectvalue1' style='width:85%;' /><input type='checkbox' class='effectpc'  name='effectpercent1' />%</td></tr>";
		echo "<tr><td><select name='effect2' style='width:100%;'>";
		include 'effectopts.php';
		echo "</select></td><td><input type='text' value='0'  name='effectvalue2' style='width:85%;' /><input type='checkbox' class='effectpc'  name='effectpercent2' />%</td></tr>";
		echo "<tr><td><select name='effect3' style='width:100%;'>";
		include 'effectopts.php';
		echo "</select></td><td><input type='text' value='0' name='effectvalue3' style='width:85%;' /><input type='checkbox' class='effectpc'  name='effectpercent3' />%</td></tr>";
		echo "<tr><td><select name='effect4' style='width:100%;'>";
		include 'effectopts.php';
		echo "</select></td><td><input type='text' value='0' name='effectvalue4'  style='width:85%;'/><input type='checkbox' class='effectpc'  name='effectpercent4' />%</td></tr>";
		echo "<tr><td><select name='effect5' style='width:100%;'>";
		include 'effectopts.php';
		echo "</select></td><td><input type='text' value='0' name='effectvalue5' style='width:85%;' /><input type='checkbox' class='effectpc'  name='effectpercent5' />%</td></tr>";
		echo "<tr><td><select name='effect6' style='width:100%;'>";
		include 'effectopts.php';
		echo "</select></td><td><input type='text' value='0' name='effectvalue6' style='width:85%;' /><input type='checkbox' class='effectpc'  name='effectpercent6' />%</td></tr>";
		echo "<tr><td><select name='effect7' style='width:100%;'>";
		include 'effectopts.php';
		echo "</select></td><td><input type='text' value='0' name='effectvalue7' style='width:85%;' /><input type='checkbox' class='effectpc'  name='effectpercent7' />%</td></tr>";
		echo "<tr><td><select name='effect8' style='width:100%;'>";
		include 'effectopts.php';
		echo "</select></td><td><input type='text' value='0' name='effectvalue8'  style='width:85%;'/><input type='checkbox' class='effectpc'  name='effectpercent8' />%</td></tr>";
		echo "<tr><td colspan='2'><button type='submit'>Submit</button></td></tr>";
		echo "</table></form></div><br /><br /><br />";
		echo "<div class='footer'><footer>Number Of Mixes: $activemix Of $count, Next Available: $lastmix</div>";
		fclose($fOpen);
		
	}
	else
	{
		echo "<h3> File Not Found </h3>";
	}
}
elseif ($mode == 'edit') 
{
	require 'posthandling.php'; //All the post values assigned to variables, in separate file to keep this file tidy(er).
	$Newmix = "";
	/*  The hex value for each item effected by the mix.  */
	$item1hex = itemhex($item1);
	$Newmix .= $item1hex;
	$item2hex = itemhex($item2);
	$Newmix .= $item2hex;
	$item3hex = itemhex($item3);
	$Newmix .= $item3hex;
	$item4hex = itemhex($item4);
	$Newmix .= $item4hex;
	$item5hex = itemhex($item5);
	$Newmix .= $item5hex;
	$item6hex = itemhex($item6);
	$Newmix .= $item6hex;
	$item7hex = itemhex($item7);
	$Newmix .= $item7hex;
	$item8hex = itemhex($item8);
	$Newmix .= $item8hex;
	$item9hex = itemhex($item9);
	$Newmix .= $item9hex;
	$item10hex = itemhex($item10);
	$Newmix .= $item10hex;
	/*  Quantities for each sheltom to be used  */
	$lucidyhex = chr($lucidy).chr(00).chr(00).chr(00);
	$Newmix .= $lucidyhex;
	$sereneohex = chr($sereneo).chr(00).chr(00).chr(00);
	$Newmix .= $sereneohex;
	$fadeohex = chr($fadeo).chr(00).chr(00).chr(00);
	$Newmix .= $fadeohex;
	$sparkyhex = chr($sparky).chr(00).chr(00).chr(00);
	$Newmix .= $sparkyhex;
	$raidenthex = chr($raident).chr(00).chr(00).chr(00);
	$Newmix .= $raidenthex;
	$transparohex = chr($transparo).chr(00).chr(00).chr(00);
	$Newmix .= $transparohex;
	$murkyhex = chr($murky).chr(00).chr(00).chr(00);
	$Newmix .= $murkyhex;
	$devinehex = chr($devine).chr(00).chr(00).chr(00);
	$Newmix .= $devinehex;
	$celestohex = chr($celesto).chr(00).chr(00).chr(00);
	$Newmix .= $celestohex;
	/*  Values for each effect   */
	$effect1hex = effecthex($effect1);
	$Newmix .= $effect1hex;
	$effect2hex = effecthex($effect2);
	$Newmix .= $effect2hex;
	$effect3hex = effecthex($effect3);
	$Newmix .= $effect3hex;
	$effect4hex = effecthex($effect4);
	$Newmix .= $effect4hex;
	$effect5hex = effecthex($effect5);
	$Newmix .= $effect5hex;
	$effect6hex = effecthex($effect6);
	$Newmix .= $effect6hex;
	$effect7hex = effecthex($effect7);
	$Newmix .= $effect7hex;
	$effect8hex = effecthex($effect8);
	$Newmix .= $effect8hex;
	/*  Values for each effect  */
	$value1hex = valuehex($effectvalue1);
	$Newmix .= $value1hex;
	$value2hex = valuehex($effectvalue2);
	$Newmix .= $value2hex;
	$value3hex = valuehex($effectvalue3);
	$Newmix .= $value3hex;
	$value4hex = valuehex($effectvalue4);
	$Newmix .= $value4hex;
	$value5hex = valuehex($effectvalue5);
	$Newmix .= $value5hex;
	$value6hex = valuehex($effectvalue6);
	$Newmix .= $value6hex;
	$value7hex = valuehex($effectvalue7);
	$Newmix .= $value7hex;
	$value8hex = valuehex($effectvalue8);
	$Newmix .= $value8hex;
	/*  Sets the Percentage indicator for the effect  */
	$pc1hex = ($effectpercent1) ? chr(2).chr(0).chr(0).chr(0): chr(1).chr(0).chr(0).chr(0);
	$Newmix .= $pc1hex;
	$pc2hex = ($effectpercent2) ? chr(2).chr(0).chr(0).chr(0): chr(1).chr(0).chr(0).chr(0);
	$Newmix .= $pc2hex;
	$pc3hex = ($effectpercent3) ? chr(2).chr(0).chr(0).chr(0): chr(1).chr(0).chr(0).chr(0);
	$Newmix .= $pc3hex;
	$pc4hex = ($effectpercent4) ? chr(2).chr(0).chr(0).chr(0): chr(1).chr(0).chr(0).chr(0);
	$Newmix .= $pc4hex;
	$pc5hex = ($effectpercent5) ? chr(2).chr(0).chr(0).chr(0): chr(1).chr(0).chr(0).chr(0);
	$Newmix .= $pc5hex;
	$pc6hex = ($effectpercent6) ? chr(2).chr(0).chr(0).chr(0): chr(1).chr(0).chr(0).chr(0);
	$Newmix .= $pc6hex;
	$pc7hex = ($effectpercent7) ? chr(2).chr(0).chr(0).chr(0): chr(1).chr(0).chr(0).chr(0);
	$Newmix .= $pc7hex;
	$pc8hex = ($effectpercent8) ? chr(2).chr(0).chr(0).chr(0): chr(1).chr(0).chr(0).chr(0);
	$Newmix .= $pc8hex;
	$desSplit = str_split($MixDes, 1);
	$MixDes = "";
	/*  Uses Pipe "|" to identify line break.  */
	foreach ($desSplit as $chr)
	{
		if ($chr == "|") {
			$MixDes .= chr(0x0D);
		}
		else
		{
			$MixDes .= $chr;
		}
	}
	/*  Adds line break at the end of the mix description  */
	if ($MixDes != "") 
	{
		$MixDes = str_pad($MixDes,strlen($MixDes) + 1, chr(0x0D), STR_PAD_RIGHT);
	}
	/*  Bulks out description to the full length  */
	$deshex = str_pad($MixDes,256, chr(00), STR_PAD_RIGHT);
	$Newmix .= $deshex;
	/*  marker for where the mix starts.  */
	$offset = $start + (($MixNo - 1)* $size);
	if (is_file($server))  // Checks file exists.
	{
		$serverread = fread(fopen($server, 'r'), filesize($server));
		$premix = substr($serverread, 0, $offset); //All code before the mix being edited.
		$postmix = substr($serverread, $offset + $size); //All code following the edited mix
		$fopen = fopen($writefile,'w');
		fwrite($fopen, $premix.$Newmix.$postmix);  // rewrites the entire file.
		fclose($fopen);
		if ($MixDes != "" && $item1 != "") 
		{	
			header("refresh:3;url=?goto=$MixNo");
			echo "<br /><div><br /><b>Mix Edit Complete, Loading in 3 seconds... <br /> Here's the Mix String: </b><br /><br /><div id='mixstring'>$Newmix</div><br /><br /></div>";
		} 
		elseif ($MixDes == "" && $item1 == "")
		{
			header("refresh:3;url=?goto=".($MixNo-1));
			echo "<br /><div><br /><b>Mix $MixNo Successfully Deleted! Loading in 3 seconds...</b><br /><br /></div>";
		}
	}
	else
	{
		echo "<div><h3>File Not Found!</h3> 
		But here is the mix string anyway... $Newmix <br /><br /></div>";
	}
}
?>