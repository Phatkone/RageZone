<?php
/**
* Functions for Mix Reader
* Written By Phatkone/EuphoriA
**/
/*  Turns the hex value into a numerical value.  */
function decval($value) {
	if (strlen($value) < 8) {
        $value = str_pad($value, 8, "0", STR_PAD_LEFT);
    }
	$val = "";
	$split = str_split($value, 2);
	if(hexdec($split[3]) < hexdec(40)) {
		switch (strtoupper($value)) {
			case "0000193F": $val = 0; break;
			case "CDCC4C3E": $val = 0.2; break;
			case "9A99993E": $val = 0.3; break;
			case "CDCCCC3E": $val = 0.4; break;
 			case "0000003F": $val = 0.5; break;
			case "9A99193F": $val = 0.6; break;
			case "CDCC4C3F": $val = 0.8; break;
			case "0000803F": $val = 1; break;
			case "9A99993F": $val = 1.2; break;
			case "0000C03F": $val = 1.5; break;
		}
	} elseif ($split[3] == 40) {
		$val = 2;
		$hex = 0;
		while ($hex < hexdec($split[2])) {
			if ($val < 4) {
			    $val += 0.25;
		    	$hex += 16;
			} elseif ($val >= 4) {
			    $val += 0.25;
			    $hex += 8;
			}
		}
	} elseif ($split[3] == 41) {
		$val = 8;
		$hex = 0;
		while ($hex < hexdec($split[2])) {
			if ($val < 16) {
			    $val += 0.5;
		    	$hex += 8;
			} elseif ($val >= 16) {
			    $val += 0.5;
			    $hex += 4;
			} 
		}
	} elseif ($split[3] == 42) {
		$val = 32;
		$hex = 0;
		while ($hex < hexdec($split[2])) {
			if ($val < 64) {
			    $val += 0.5;
		    	$hex += 2;
			} elseif ($val >= 64) {
			    $val += 0.5;
			    $hex += 1;
			} 
		}
	} 
return $val;
}
/*  Turns Item values into item names  */
function items($item) {
	$split = str_split($item,1);
	$val = "";
	foreach ($split as $char) {
		$val .= dechex(ord($char));
	}
	switch ($val) {
		case "11": $val = "Axe"; break;
		case "21": $val = "Claw"; break;
		case "31": $val = "Mace"; break;
		case "41": $val = "Wand"; break;
		case "51": $val = "Scythe"; break;
		case "61": $val = "Bow"; break;
		case "71": $val = "Sword"; break;
		case "81": $val = "Javelin"; break;
		case "91": $val = "Dagger"; break;
		case "a1": $val = "Phantom"; break;
		case "12": $val = "Armor"; break;
		case "22": $val = "Boots"; break;
		case "32": $val = "Gloves"; break;
		case "42": $val = "Shield"; break;
		case "52": $val = "Robe"; break;
		case "13": $val = "Amulet"; break;
		case "23": $val = "Armlet"; break;
		case "33": $val = "Orb"; break;
		case "43": $val = "Ring"; break;
		/*  NFI what these two item codes relate to, but the mixes for them are all over the shop, I've left them out for that reason, mixes 60~ to 100~  */
		//case "122": $val = "Something..."; break;
		//case "132": $val = "Something...";break;
		default: $val = ""; break;
	}
	return $val;
}
/*  Turns Item back to Hex  */
function itemhex($item) {
	switch ($item) {
		case "axe": $val = chr(00).chr(00).chr(1).chr(01) ;break;
		case "claw": $val = chr(00).chr(00).chr(2).chr(01) ;break;
		case "mace": $val = chr(00).chr(00).chr(3).chr(01) ;break;
		case "wand": $val = chr(00).chr(00).chr(4).chr(01) ;break;
		case "scythe": $val = chr(00).chr(00).chr(5).chr(01) ;break;
		case "bow": $val = chr(00).chr(00).chr(6).chr(01) ;break;
		case "sword": $val = chr(00).chr(00).chr(7).chr(01) ;break;
		case "javelin": $val = chr(00).chr(00).chr(8).chr(01) ;break;
		case "dagger": $val = chr(00).chr(00).chr(9).chr(1) ;break;
		case "phantom": $val = chr(00).chr(00).chr(0x0a).chr(01) ;break;
		case "armor": $val = chr(00).chr(00).chr(01).chr(02) ;break;
		case "boots": $val = chr(00).chr(00).chr(02).chr(02) ;break;
		case "gloves": $val = chr(00).chr(00).chr(03).chr(02) ;break;
		case "shield": $val = chr(00).chr(00).chr(04).chr(02) ;break;
		case "robe": $val = chr(00).chr(00).chr(05).chr(02) ;break;
		case "amulet": $val = chr(00).chr(00).chr(01).chr(03) ;break;
		case "armlet": $val = chr(00).chr(00).chr(02).chr(03) ;break;
		case "orb": $val = chr(00).chr(00).chr(03).chr(03) ;break;
		case "ring": $val = chr(00).chr(00).chr(04).chr(03) ;break;
		default: $val = chr(00).chr(00).chr(00).chr(00); break;
	}
	return $val;
}
/*  Turns effect values into names  */
function effect($effect) {
	$split = str_split($effect, 1);
	$val = "";
	foreach ($split as $char) {
		if (ord($char) == "0") {
			$val .= "00";
		} else {
			$val .= str_pad(dechex(ord($char)),2,"0", STR_PAD_LEFT);
		}
	}
	switch ($val) {
		case "01000000": $val = "Flame"; break;
		case "02000000": $val = "Frost"; break;
		case "04000000": $val = "Lightning"; break;
		case "08000000": $val = "Poison"; break;
		case "10000000": $val = "Organic"; break;
		case "20000000": $val = "Critical"; break;
		case "40000000": $val = "Attack Rating"; break;
		case "80000000": $val = "Min Attack"; break;
		case "00010000": $val = "Max Attack"; break;
		case "00020000": $val = "Weapon Speed"; break;
		case "00040000": $val = "Absorb"; break;
		case "00080000": $val = "Defence"; break;
		case "00100000": $val = "Block"; break;
		case "00200000": $val = "Speed"; break;
		case "00400000": $val = "HP"; break;
		case "00800000": $val = "MP"; break;
		case "00000100": $val = "STM"; break;
		case "00000200": $val = "HP Regen"; break;
		case "00000400": $val = "MP Regen"; break;
		case "00000800": $val = "STM Regen"; break;
		default: $val = "";
	}
	return $val;
}
/*  Turn effect to hex  */
function effecthex($effect) {
	switch ($effect) {
		case "flame": $val = chr(1).chr(00).chr(00).chr(00); break;
		case "frost": $val = chr(2).chr(00).chr(00).chr(00); break;
		case "lightning": $val = chr(4).chr(00).chr(00).chr(00); break;
		case "poison": $val = chr(8).chr(00).chr(00).chr(00); break;
		case "organic": $val = chr(0x10).chr(00).chr(00).chr(00); break;
		case "critical": $val = chr(0x20).chr(00).chr(00).chr(00); break;
		case "ar": $val = chr(0x40).chr(00).chr(00).chr(00); break;
		case "minatk": $val = chr(0x80).chr(00).chr(00).chr(00); break;
		case "maxatk": $val = chr(00).chr(1).chr(00).chr(00); break;
		case "wepspd": $val = chr(00).chr(2).chr(00).chr(00); break;
		case "absorb": $val = chr(00).chr(4).chr(00).chr(00); break;
		case "defence": $val = chr(00).chr(8).chr(00).chr(00); break;
		case "block": $val = chr(00).chr(0x10).chr(00).chr(00); break;
		case "speed": $val = chr(00).chr(0x20).chr(00).chr(00); break;
		case "hp": $val = chr(00).chr(0x40).chr(00).chr(00); break;
		case "mp": $val = chr(00).chr(0x80).chr(00).chr(00); break;
		case "stm": $val = chr(00).chr(00).chr(1).chr(00); break;
		case "hpreg": $val = chr(00).chr(00).chr(2).chr(00); break;
		case "mpreg": $val = chr(00).chr(00).chr(4).chr(00); break;
		case "stmreg": $val = chr(00).chr(00).chr(8).chr(00); break;
		default: $val = chr(00).chr(00).chr(00).chr(00); break;
	}
	return $val;
}
/*  Returns hex as value  */
function value($value) {
	global $valuelist;
	$split = str_split($value, 1);
	$num = "";
	foreach ($split as $val) {
		$num .= dechex(ord($val));
	}
	$val = decval($num);
	return $val;
}
/*  Converts Value To Hex  */
function valuehex($value) {
	if ($value < 2) {
		switch (strtoupper($value)) {
			case 0: $val = chr(00).chr(00).chr(00).chr(00); break;
			case 0.2: $val = chr(0xCD).chr(0xCC).chr(0x4C).chr(0x3E); break;
			case 0.3: $val = chr(0x9A).chr(0x99).chr(0x99).chr(0x3E); break;
			case 0.4: $val = chr(0xCD).chr(0xCC).chr(0xCC).chr(0x3E); break;
			case 0.5: $val = chr(00).chr(00).chr(00).chr(0x3F); break;
			case 0.6: $val = chr(0x9A).chr(0x99).chr(0x19).chr(0x3F); break;
			case 0.8: $val = chr(0xCD).chr(0xCC).chr(0x4C).chr(0x3F); break;
			case 1: $val = chr(00).chr(00).chr(0x80).chr(0x3F); break;
			case 1.2: $val = chr(0x9A).chr(0x99).chr(0x99).chr(0x3F); break;
			case 1.5: $val = chr(00).chr(00).chr(0xC0).chr(0x3F); break;
			default: $val = chr(00).chr(00).chr(00).chr(00); break;
		}
		return $val;
	}
	elseif ($value >= 2 && $value < 4) {
		$multi = 0x40;
		$i = 2; $hex = 0;
		while ($i < $value) {
			$i += 0.25;
			$hex += 0xF;
		}
	}
	elseif ($value >= 4 && $value < 8) {
		$multi = 0x40;
		$i = 4; $hex = 0x80;
		while ($i < $value) {
			$i += 0.25;
			$hex += 0x8;
		}
	}
	elseif ($value >= 8 && $value < 16) {
		$multi = 0x41;
		$i = 8; $hex = 0;
		while ($i < $value) {
			$i += 0.25;
			$hex += 0x4;
		}
	}
	elseif ($value >= 16 && $value < 32) {
		$multi = 0x41;
		$i = 16; $hex = 0x80;
		while ($i < $value) {
			$i += 0.25;
			$hex += 0x2;
		}
	}
	elseif ($value >= 32 && $value <= 63) {
		$multi = 0x42;
		$i = 32; $hex = 0;
		while ($i < $value) {
			$i += 0.25;
			$hex += 0x1;
		}
	}
	elseif ($value >= 64) {
		$multi = 0x42;
		$i = 64; $hex = 0x80;
		while ($i < $value) {
			$i += 0.5;
			$hex += 0x1;
		}
	}
	$val = chr(00).chr(00).chr($hex).chr($multi);
	return $val;
}
/* Convert percentage ON/OFF */
function percent($percent) {
	$val = dechex(ord($percent));
	switch ($val) {
		case "01": $val = ""; break;
		case "02": $val = "%"; break;
		default: $val = "";
	}
	return $val;
}
/*  Percent to Hex  */
function pchex($percent) {
	switch ($percent) {
		case true: $val = chr(00).chr(00).chr(00).chr(02); break;
		case false: $val = chr(00).chr(00).chr(00).chr(01); break;
		default: $val = chr(00).chr(00).chr(00).chr(01); break;
	}
	return $val;
}
/*  Arrays to swap values  */
function effectarr($effect) {
	switch ($effect) {
		case "N/A": $ret = ""; break;
		case "Flame": $ret = "flame"; break;
		case "Frost": $ret = "frost"; break;
		case "Lightning": $ret = "lightning"; break;
		case "Poison": $ret = "poison"; break;
		case "Organic": $ret = "organic"; break;
		case "Critical": $ret = "critical"; break;
		case "Attack Rating": $ret = "ar"; break;
		case "Min Attack": $ret = "minatk"; break;
		case "Max Attack": $ret = "maxatk"; break;
		case "Weapon Speed": $ret = "wepspd"; break;
		case "Absorb": $ret = "absorb"; break;
		case "Defence": $ret = "defence"; break;
		case "Block": $ret = "block"; break;
		case "Speed": $ret = "speed"; break;
		case "HP": $ret = "hp"; break;
		case "MP": $ret = "mp"; break;
		case "STM": $ret = "stm"; break;
		case "HP Regen": $ret = "hpreg"; break;
		case "MP Regen": $ret = "mpreg"; break;
		case "STM Regen": $ret = "stmreg"; break;
		default: $ret = "N/A"; break;
	}
	return $ret;
}
?>