/**
*	@Author - EuphoriA AKA Phatkone AKA Ashikabi
*	@Version - 1.0
*
*	About:
*
*	The below script creates the function "Is_Blocked"
*	The function takes one value, an account name.
*	
*
*	Usage:
*
*   Execute this script to create the function, if the function exists
*	then uncomment the two top lines (remove "--") to drop the function prior to creation
*	
*	To use function just call it with an account name. 
*		I.E. "SELECT dbo.Is_Blocked('Ashikabi') AS 'Block Status'"
*
*	Will return one of 3 results:
*		Blocked
*		Not Blocked
*		Unexpected Result
**/

--DROP FUNCTION Is_Blocked
--GO
CREATE FUNCTION Is_Blocked (@userid NVARCHAR(50))
RETURNS NVARCHAR(50)
AS

BEGIN
DECLARE @db NVARCHAR(50) = SUBSTRING(@userid,1,1);
	DECLARE @query INT = 
	CASE LOWER(@db)
		WHEN 'a' THEN (SELECT BlockChk FROM accountdb.dbo.agameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'b' THEN (SELECT BlockChk FROM accountdb.dbo.bgameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'c' THEN (SELECT BlockChk FROM accountdb.dbo.cgameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'd' THEN (SELECT BlockChk FROM accountdb.dbo.dgameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'e' THEN (SELECT BlockChk FROM accountdb.dbo.egameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'f' THEN (SELECT BlockChk FROM accountdb.dbo.fgameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'g' THEN (SELECT BlockChk FROM accountdb.dbo.ggameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'h' THEN (SELECT BlockChk FROM accountdb.dbo.hgameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'i' THEN (SELECT BlockChk FROM accountdb.dbo.igameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'j' THEN (SELECT BlockChk FROM accountdb.dbo.jgameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'k' THEN (SELECT BlockChk FROM accountdb.dbo.kgameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'l' THEN (SELECT BlockChk FROM accountdb.dbo.lgameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'm' THEN (SELECT BlockChk FROM accountdb.dbo.mgameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'n' THEN (SELECT BlockChk FROM accountdb.dbo.ngameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'o' THEN (SELECT BlockChk FROM accountdb.dbo.ogameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'p' THEN (SELECT BlockChk FROM accountdb.dbo.pgameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'q' THEN (SELECT BlockChk FROM accountdb.dbo.qgameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'r' THEN (SELECT BlockChk FROM accountdb.dbo.rgameuser WHERE userid LIKE ''+@userid+'')
		WHEN 's' THEN (SELECT BlockChk FROM accountdb.dbo.sgameuser WHERE userid LIKE ''+@userid+'')
		WHEN 't' THEN (SELECT BlockChk FROM accountdb.dbo.tgameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'u' THEN (SELECT BlockChk FROM accountdb.dbo.ugameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'v' THEN (SELECT BlockChk FROM accountdb.dbo.vgameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'w' THEN (SELECT BlockChk FROM accountdb.dbo.wgameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'x' THEN (SELECT BlockChk FROM accountdb.dbo.xgameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'y' THEN (SELECT BlockChk FROM accountdb.dbo.ygameuser WHERE userid LIKE ''+@userid+'')
		WHEN 'z' THEN (SELECT BlockChk FROM accountdb.dbo.zgameuser WHERE userid LIKE ''+@userid+'')
	END
	--IF ((SELECT BlockChk FROM accountdb.dbo.agameuser WHERE userid LIKE ''+@userid+'') <> 0)
	IF (@query <> 0)
		RETURN 'Blocked'
	ELSE IF (@query <> 1)
		RETURN 'Not Blocked'
RETURN 'Unexpected Result'
END