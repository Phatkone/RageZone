/**
*	@Author - EuphoriA AKA Phatkone AKA Ashikabi
*	@Version - 1.0
*
*	About:
*
*	The below script reads through all account gameuser tables returning any account 
*	that is blocked.
*
*	Usage:
*
*   Execute this script, results are laid out in an easy to view manner,
*	showing table name, account name and blocked status.
*
**/
USE AccountDB
GO
DECLARE @chars TABLE (ID INT IDENTITY(1,1), Characters NVARCHAR(50) PRIMARY KEY)
DECLARE @asciiCode INT=65 WHILE @asciiCode<= 89 
BEGIN
	--SET FMTONLY ON
	INSERT @chars(Characters) SELECT CHAR(@asciiCode)+'GameUser'
	SELECT @asciiCode = @asciiCode+1
	--SET FMTONLY OFF
END

DECLARE @char NVARCHAR(50)
DECLARE @query NVARCHAR(MAX) = '';
DECLARE chars CURSOR FOR
	SELECT Characters FROM @chars
OPEN chars
FETCH NEXT FROM chars INTO @char
WHILE @@FETCH_STATUS = 0
BEGIN
	SET @query += 'SELECT userid, Blocked = CASE blockchk WHEN 1 THEN ''Blocked'' WHEN 0 THEN ''Not Blocked'' END , '''+@char+''' AS "Table Name" FROM '+@char+' WHERE blockchk <> 0 
			   UNION
			   ';
	FETCH NEXT FROM chars INTO @char
END
SET @query += 'SELECT userid,  Blocked = CASE blockchk WHEN 1 THEN ''Blocked'' WHEN 0 THEN ''Not Blocked'' END, '''+@char+''' AS "Table Name" FROM ZGameUser WHERE blockchk <> 0'
EXEC(''+@query+'');
CLOSE chars
DEALLOCATE chars
