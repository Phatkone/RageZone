/**
*	@Author - EuphoriA AKA Phatkone AKA Ashikabi
*	@Version - 1.0
*
*	About:
*
*	The below script is to create a procedure to check on the block status of an account.
*	Run this script once to create the procedure. You do not need to run it every time.
*
*	Usage:
*
*   Enter account name in first field (not case sensitive)
*   Enter action in second field (block, unblock, check) (also not case sensitive)
*   Default action is check, if only one parametre is defined it will default to unblock.
*   userid ane action MUST be encapsulated in single quotations ('')
*   Example use:
*
*   Check:
*       EXEC AccountDB.dbo.BLOCK 'Name', 'check';
*   OR
*       EXEC AccountDB.dbo.BLOCK 'Name';
*
*   Block:
*       EXEC AccountDB.dbo.BLOCK 'Name', 'block';
*
*   Unblock:
*       EXEC AccountDB.dbo.BLOCK 'Name', 'unblock';
**/

USE AccountDB
GO
/**
*	If the procedure already exists you'll need to drop it first.
*	Do so with the following lines:
*	Remove the "--" to enable the lines.
*/
--DROP PROCEDURE BLOCK
--GO

/**
*	Creates the procedure.
**/
CREATE PROCEDURE BLOCK (@userid NVARCHAR(50), @action NVARCHAR(50) = 'unblock') AS
DECLARE @table NVARCHAR(50) = SUBSTRING(@userid,1,1)+'gameuser';
BEGIN 
	IF @action LIKE 'block'
	BEGIN 
		EXEC('UPDATE AccountDB.dbo.'+@table+' SET BlockChk=1 WHERE userid LIKE '''+@userid+'''');
	END
	ELSE IF @action LIKE 'check'
	BEGIN
		EXEC('
		DECLARE @res INT = (SELECT BlockChk FROM AccountDB.dbo.'+@table+' WHERE userid LIKE '''+@userid+''')
		IF @res = 1
			SELECT ''Blocked'' AS '''+@userid+'''
		ELSE
			
			SELECT ''Not Blocked'' AS '''+@userid+'''
		');
	END
	ELSE
	BEGIN
		EXEC('UPDATE AccountDB.dbo.'+@table+' SET BlockChk=0 WHERE userid LIKE '''+@userid+'''');
	END
END