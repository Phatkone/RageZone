/**
*
*	@Author - EuphoriA AKA Phatkone AKA Ashikabi
*	@Version - 1.0
*
*	Creates the Database and tables for AccountDB and the process SQL login account
*
**/
IF NOT EXISTS (SELECT loginname FROM master.sys.syslogins WHERE name = 'ptsql')
BEGIN
	CREATE LOGIN ptsql WITH PASSWORD = 'PristontaleSQLAcc3ss', CHECK_POLICY = OFF;
END

IF ((SELECT COUNT(*) FROM master.sys.databases WHERE name LIKE 'AccountDB') <> 0)
	RAISERROR('Table Already Exists. Please Delete It And Try Again', 20, -1) WITH LOG
	
CREATE DATABASE [AccountDB]
GO

USE [AccountDB]
GO

CREATE USER ptsql FROM LOGIN ptsql
GO

GRANT SELECT, UPDATE, DELETE, INSERT TO ptsql
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[allGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [allUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[aGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [aUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[bGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [bUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[cGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [cUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[dGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [dUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[eGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [eUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[fGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [fUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[gGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [gUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[hGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [hUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[iGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [iUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[jGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [jUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[kGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [kUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[lGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [lUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[mGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [mUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[nGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [nUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[oGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [oUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[pGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [pUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[qGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [qUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[rGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [rUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[sGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [sUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[tGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [tUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[uGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [uUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[vGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [vUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[wGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [wUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[xGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [xUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[yGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [yUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[zGameUser](
	[userid] [varchar](32) NOT NULL,
	[Passwd] [varchar](32) NOT NULL,
	[GameCode] [varchar](10) NULL,
	[GPCode] [varchar](10) NULL,
	[RegistDay] [datetime] NULL,
	[DisuseDay] [datetime] NULL,
	[UsePeriod] [int] NULL,
	[inuse] [char](1) NOT NULL,
	[Grade] [char](1) NOT NULL,
	[EventChk] [char](1) NOT NULL,
	[SelectChk] [char](1) NOT NULL,
	[BlockChk] [char](1) NOT NULL,
	[SpecialChk] [char](1) NOT NULL,
	[ServerName] [varchar](50) NULL,
	[Credit] [char](1) NOT NULL,
	[ECoin] [money] NULL,
	[StartDay] [datetime] NULL,
	[LastDay] [datetime] NULL,
	[EditDay] [datetime] NULL,
	[RNo] [int] NULL,
	[DelChk] [char](1) NOT NULL,
	[SNo] [varchar](50) NULL,
	[Channel] [varchar](50) NULL,
	[BNum] [int] NULL,
	[MXServer] [varchar](50) NULL,
	[MXChar] [varchar](50) NULL,
	[MXType] [int] NULL,
	[MXLevel] [int] NULL,
	[MXExp] [bigint] NULL,
 CONSTRAINT [zUser_pk] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


