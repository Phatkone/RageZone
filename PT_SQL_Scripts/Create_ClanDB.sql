/**
*
*	@Author - EuphoriA AKA Phatkone AKA Ashikabi
*	@Version - 1.0
*
*	Creates the Database and tables for ClanDB
*
**/
IF ((SELECT COUNT(*) FROM master.sys.databases WHERE name LIKE 'ClanDB') <> 0)
	RAISERROR('Table Already Exists. Please Delete It And Try Again', 20, -1) WITH LOG
CREATE DATABASE [ClanDB]
GO

USE [ClanDB]
GO

CREATE USER ptsql FROM LOGIN ptsql
GO

GRANT SELECT, UPDATE, DELETE, INSERT TO ptsql
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CL](
	[IDX] [int] NOT NULL,
	[ClanName] [nvarchar](50) NOT NULL,
	[Note] [text] NOT NULL,
	[NoteCnt] [int] NOT NULL,
	[UserID] [nvarchar](50) NULL,
	[ClanZang] [nvarchar](50) NOT NULL,
	[Flag] [int] NOT NULL,
	[MemCnt] [int] NOT NULL,
	[MIconCnt] [int] NOT NULL,
	[RegiDate] [datetime] NOT NULL,
	[LimitDate] [datetime] NOT NULL,
	[DelActive] [char](1) NOT NULL,
	[PFlag] [int] NOT NULL,
	[KFlag] [int] NOT NULL,
	[Cpoint] [int] NULL,
	[CWin] [int] NULL,
	[CFail] [int] NULL,
	[ClanMoney] [bigint] NULL,
	[CNFlag] [int] NULL,
	[SiegeMoney] [bigint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

CREATE TABLE [dbo].[CT](
	[SNo] [int] NULL,
	[ServerName] [varchar](50) NULL,
	[MIDX] [int] NULL,
	[ClanName] [varchar](50) NULL,
	[ClanJang] [int] NULL,
	[ClanImage] [varchar](50) NULL,
	[UserID] [varchar](50) NULL,
	[ChName] [varchar](50) NULL,
	[GPCode] [varchar](20) NULL,
	[LogonTime] [datetime] NULL,
	[IP] [varchar](20) NULL,
	[RNo] [int] NULL,
	[Flag] [int] NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[LI](
	[IMG] [int] NOT NULL,
	[ID] [int] NOT NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[UL](
	[IDX] [int] NOT NULL,
	[MIDX] [int] NOT NULL,
	[userid] [nvarchar](50) NOT NULL,
	[ChName] [nvarchar](50) NOT NULL,
	[ChType] [int] NULL,
	[ChLv] [int] NULL,
	[ClanName] [nvarchar](50) NULL,
	[Permi] [char](2) NOT NULL,
	[JoinDate] [datetime] NOT NULL,
	[DelActive] [char](1) NOT NULL,
	[PFlag] [int] NOT NULL,
	[KFlag] [int] NOT NULL,
	[MIconCnt] [int] NOT NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[SeigeMoneyTax] (
	[idx] [bigint] NOT NULL,
	[mixing] [bigint] NULL,
	[aging] [bigint] NULL,
	[shop] [bigint] NULL,
	[poison1] [bigint] NULL,
	[poison2] [bigint] NULL,
	[poison3] [bigint] NULL,
	[force] [bigint] NULL,
	[warpgate] [bigint] NULL,
	[skill] [bigint] NULL,
	[total] [bigint] NULL,
	[tax] [bigint] NULL,
	[servername] [nvarchar](50) NULL	
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

