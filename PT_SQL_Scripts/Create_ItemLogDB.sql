/**
*
*	@Author - EuphoriA AKA Phatkone AKA Ashikabi
*	@Version - 1.0
*
*	Creates the database and tables for SoDDB
*
**/
IF ((SELECT COUNT(*) FROM master.sys.databases WHERE name LIKE 'ItemLogDB') <> 0)
	RAISERROR('Table Already Exists. Please Delete It And Try Again', 20, -1) WITH LOG
CREATE DATABASE [ItemLogDB]
GO

USE [ItemLogDB]
GO

CREATE USER svc_ptsql FROM LOGIN svc_ptsql
GO

GRANT SELECT, UPDATE, DELETE, INSERT TO svc_ptsql
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [ItemLogDB].[dbo].[il](
    [userid] [nvarchar](50) NOT NULL,
    [charname] [nvarchar](50) NOT NULL,
    [ip] [nvarchar](15) NOT NULL,
    [flag] [int] NULL,
    [tuserid] [nvarchar](50) NULL,
    [tcharname] [nvarchar](50) NULL,
    [tmoney] [int] NULL,
    [tip] [nvarchar](15) NULL,
    [itemcount] [int] NULL,
    [itemcode] [bigint] NULL,
    [itemino] [int] NULL,
    [itemino_1] [int] NULL,
    [registday] [datetime] NULL
) ON [PRIMARY]
GO
CREATE TABLE [ItemLogDB].[dbo].[ItemLog](
    [userid] [nvarchar](50) NOT NULL,
    [charname] [nvarchar](50) NOT NULL,
    [ip] [nvarchar](15) NOT NULL,
    [flag] [int] NULL,
    [tuserid] [nvarchar](50) NULL,
    [tcharname] [nvarchar](50) NULL,
    [tmoney] [int] NULL,
    [tip] [nvarchar](15) NULL,
    [itemcount] [int] NULL,
    [itemcode] [bigint] NULL,
    [itemino] [int] NULL,
    [itemino_1] [int] NULL,
    [registday] [datetime] NULL
) ON [PRIMARY]
GO

SET ANSI_PADDING OFF
GO

