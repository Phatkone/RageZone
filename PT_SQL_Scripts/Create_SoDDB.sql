/**
*
*	@Author - EuphoriA AKA Phatkone AKA Ashikabi
*	@Version - 1.0
*
*	Creates the database and tables for SoDDB
*
**/
IF ((SELECT COUNT(*) FROM master.sys.databases WHERE name LIKE 'SoDDB') <> 0)
	RAISERROR('Table Already Exists. Please Delete It And Try Again',20,-1) WITH LOG

CREATE DATABASE [SoDDB]
GO

USE [SoDDB]
GO

CREATE USER ptsql FROM LOGIN ptsql
GO

GRANT SELECT, DELETE, INSERT, UPDATE TO ptsql
GO
/**
*
*	Create Static Tables
*
**/
CREATE TABLE [dbo].[Levellist](
	[CharName] [nvarchar](50) NULL,
	[CharLevel] [int] NULL,
	[CharClass] [nvarchar](50) NULL,
	[ID] [nvarchar](50) NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[SoD2BBSSub](
	[SIDX] [int] NOT NULL,
	[MINDEX] [int] NOT NULL,
	[Userid] [varchar](25) NOT NULL,
	[WriteName] [varchar](100) NOT NULL,
	[Content] [text] NOT NULL,
	[RegiDate] [datetime] NOT NULL,
	[RegiIP] [char](15) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

CREATE TABLE [dbo].[Sod2Config](
	[MIDX] [int] NULL,
	[ClanName] [varchar](50) NULL,
	[UserID] [varchar](50) NULL,
	[CharName] [varchar](50) NULL,
	[SelServer] [int] NOT NULL,
	[ServerName] [varchar](50) NULL,
	[TaxRates] [int] NOT NULL,
	[OperFrom] [datetime] NOT NULL,
	[OperTo] [datetime] NOT NULL,
	[TotalEnterMoney] [bigint] NULL,
	[BeforeMoney] [bigint] NULL,
	[TodayMoney] [bigint] NULL,
	[TotalMoney] [bigint] NULL,
	[TotalEnterNum] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[Note] [text] NULL,
	[MIDX1] [int] NULL,
	[MIDX2] [int] NULL,
	[M0money] [bigint] NULL,
	[M1money] [bigint] NULL,
	[M2money] [bigint] NULL,
	[VInfo] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

CREATE TABLE [dbo].[Sod2ConfigLog](
	[IDX] [int] NOT NULL,
	[MIDX] [int] NULL,
	[ClanName] [varchar](50) NULL,
	[UserID] [varchar](50) NULL,
	[CharName] [varchar](50) NULL,
	[SelServer] [int] NOT NULL,
	[ServerName] [varchar](50) NOT NULL,
	[TaxRates] [int] NOT NULL,
	[OperFrom] [datetime] NOT NULL,
	[OperTo] [datetime] NOT NULL,
	[TotalEnterMoney] [bigint] NULL,
	[BerforeMoney] [bigint] NULL,
	[TodayMoney] [bigint] NULL,
	[TotalMoney] [bigint] NULL,
	[TotalEnterNum] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[Note] [text] NULL,
	[MIDX1] [int] NULL,
	[MIDX2] [int] NULL,
	[M0money] [bigint] NULL,
	[M1money] [bigint] NULL,
	[M2money] [bigint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

CREATE TABLE [dbo].[SoD2Notice](
	[IDX] [int] NOT NULL,
	[MIDX] [int] NOT NULL,
	[SelServer] [int] NOT NULL,
	[UserID] [varchar](50) NOT NULL,
	[Notice] [text] NOT NULL,
	[RegiDate] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

CREATE TABLE [dbo].[SOD2RecBySandurr](
	[SNo] [int] NULL,
	[UserID] [varchar](50) NULL,
	[CharName] [varchar](50) NULL,
	[CharType] [int] NULL,
	[Point] [int] NULL,
	[KillCount] [int] NULL,
	[GLevel] [int] NULL,
	[TotalPoint] [int] NULL,
	[TotalUser] [int] NULL,
	[SuccessUser] [int] NULL,
	[ServerName] [varchar](50) NULL,
	[PCRNo] [int] NULL,
	[GPCode] [varchar](50) NULL,
	[BusinessName] [varchar](100) NULL,
	[BAddress1] [varchar](100) NULL,
	[PMNo] [int] NULL,
	[Contribute] [int] NULL,
	[RegistDay] [datetime] NULL,
	[ServerTime] [bigint] NULL,
	[Flag] [int] NULL,
	[ClanIDX] [int] NULL,
	[ClanName] [varchar](50) NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[Sod2TaxLog](
	[IDX] [int] NOT NULL,
	[MIDX] [int] NULL,
	[ClanName] [varchar](50) NULL,
	[MCount] [int] NULL,
	[UserID] [varchar](50) NULL,
	[CharName] [varchar](50) NULL,
	[SelServer] [int] NULL,
	[TaxRates] [int] NULL,
	[OperFrom] [datetime] NULL,
	[OperTo] [datetime] NULL,
	[RegiDate] [datetime] NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[SoDAllTime](
	[UserID] [nvarchar](50) NOT NULL,
	[CharType] [nvarchar](50) NOT NULL,
	[CharName] [nvarchar](50) NOT NULL,
	[Point] [int] NOT NULL,
	[KillCount] [int] NOT NULL,
	[RegistDay] [datetime] NOT NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[SoDBBSMain](
	[IDX] [int] NOT NULL,
	[Userid] [varchar](25) NOT NULL,
	[WriteName] [varchar](100) NOT NULL,
	[Title] [varchar](600) NULL,
	[Job] [int] NULL,
	[Content] [text] NOT NULL,
	[SelServer] [int] NOT NULL,
	[RegiDate] [datetime] NOT NULL,
	[RegiIP] [char](15) NOT NULL,
	[Hit] [int] NOT NULL,
	[CountCom] [int] NOT NULL,
	[OwnerID] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

CREATE TABLE [dbo].[temp](
	[IDX] [int] NOT NULL,
	[CIDX] [int] NOT NULL,
	[CServerTime] [bigint] NOT NULL,
	[Server] [int] NOT NULL,
	[ClanName] [varchar](20) NOT NULL,
	[ServerName] [varchar](50) NOT NULL,
	[CPoint] [int] NOT NULL,
	[CRegistDay] [datetime] NOT NULL,
	[ServerTime_Temp] [bigint] NULL,
	[Point_Temp] [int] NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[UserBlackList](
	[SOD2No] [int] NOT NULL,
	[UserID] [varchar](32) NOT NULL,
	[CharName] [varchar](50) NOT NULL,
	[ServerCode] [int] NOT NULL,
	[Memo] [varchar](250) NULL,
	[RegistDay] [datetime] NULL
) ON [PRIMARY]

GO