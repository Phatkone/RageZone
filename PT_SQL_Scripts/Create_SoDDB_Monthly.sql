/**
*
*	@Author - EuphoriA AKA Phatkone AKA Ashikabi
*	@Version - 1.0
*
*	Create Procedure to Create Monthly SOD Tables.
*	
*	Use:
*	To Utilise the procedure, run a query calling the procedure.
*	Example:
*		EXEC SoDDB.DBO.makeSODMonthlyTables;
*
**/
USE SoDDB
GO

CREATE PROCEDURE makeSODMonthlyTables AS
DECLARE @yymm NVARCHAR(50) = SUBSTRING(CAST(YEAR(GETDATE())AS NVARCHAR(4)),3,2) +LEFT('0'+CAST(MONTH(GETDATE()) AS NVARCHAR(2)),2);
BEGIN
	EXEC('
		SET ANSI_NULLS ON
		

		SET QUOTED_IDENTIFIER ON
		

		SET ANSI_PADDING ON
		
		IF NOT EXISTS (SELECT * FROM soddb.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE ''bsodrecord'+@yymm+''')
		BEGIN
		CREATE TABLE [SoDDB].[dbo].[bsodrecord'+@yymm+'](
			[UserID] [varchar](50) NULL,
			[CharName] [varchar](50) NULL,
			[CharType] [int] NULL,
			[Point] [int] NULL,
			[KillCount] [int] NULL,
			[GLevel] [int] NULL,
			[TotalPoint] [int] NULL,
			[TotalUser] [int] NULL,
			[SuccessUser] [int] NULL,
			[ServerName] [varchar](50) NULL,
			[PCRNo] [int] NULL,
			[GPCode] [varchar](50) NULL,
			[Contribute] [int] NULL,
			[RegistDay] [datetime] NULL,
			[BusinessName] [varchar](100) NULL,
			[BAddress1] [varchar](100) NULL,
			[PMNo] [int] NULL,
			[ServerTime] [int] NULL
		) ON [PRIMARY]
		END
		
		IF NOT EXISTS (SELECT * FROM soddb.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE ''sod'+@yymm+''')
		BEGIN
		CREATE TABLE [SoDDB].[dbo].[sod'+@yymm+'](
			[SODNo] [int] NOT NULL,
			[UserID] [varchar](50) NULL,
			[CharName] [varchar](50) NULL,
			[CharType] [int] NULL,
			[ServerName] [varchar](50) NULL,
			[Point] [int] NULL,
			[Point_Lev] [int] NULL,
			[Point_Day] [datetime] NULL,
			[Ave] [int] NULL,
			[Ave_Lev] [int] NULL,
			[Ave_Day] [datetime] NULL,
			[KillCnt] [int] NULL,
			[KillCnt_Lev] [int] NULL,
			[KillCnt_Day] [datetime] NULL,
			[Contri] [int] NULL,
			[Contri_Lev] [int] NULL,
			[Contri_Day] [datetime] NULL,
			[TotPoint] [int] NULL,
			[Cnt] [int] NULL,
			[TotCnt] [int] NULL,
			[L1_Point] [int] NULL,
			[L1_Point_Lev] [int] NULL,
			[L1_Point_Day] [datetime] NULL,
			[L1_Ave] [int] NULL,
			[L1_Ave_Lev] [int] NULL,
			[L1_Ave_Day] [datetime] NULL,
			[L1_KillCnt] [int] NULL,
			[L1_KillCnt_Lev] [int] NULL,
			[L1_KillCnt_Day] [datetime] NULL,
			[L1_Contri] [int] NULL,
			[L1_Contri_Lev] [int] NULL,
			[L1_Contri_Day] [datetime] NULL,
			[L1_TotPoint] [int] NULL,
			[L1_Cnt] [int] NULL,
			[L1_TotCnt] [int] NULL,
			[L2_Point] [int] NULL,
			[L2_Point_Lev] [int] NULL,
			[L2_Point_Day] [datetime] NULL,
			[L2_Ave] [int] NULL,
			[L2_Ave_Lev] [int] NULL,
			[L2_Ave_Day] [datetime] NULL,
			[L2_KillCnt] [int] NULL,
			[L2_KillCnt_Lev] [int] NULL,
			[L2_KillCnt_Day] [datetime] NULL,
			[L2_Contri] [int] NULL,
			[L2_Contri_Lev] [int] NULL,
			[L2_Contri_Day] [datetime] NULL,
			[L2_TotPoint] [int] NULL,
			[L2_Cnt] [int] NULL,
			[L2_TotCnt] [int] NULL,
			[L3_Point] [int] NULL,
			[L3_Point_Lev] [int] NULL,
			[L3_Point_Day] [datetime] NULL,
			[L3_Ave] [int] NULL,
			[L3_Ave_Lev] [int] NULL,
			[L3_Ave_Day] [datetime] NULL,
			[L3_KillCnt] [int] NULL,
			[L3_KillCnt_Lev] [int] NULL,
			[L3_KillCnt_Day] [datetime] NULL,
			[L3_Contri] [int] NULL,
			[L3_Contri_Lev] [int] NULL,
			[L3_Contri_Day] [datetime] NULL,
			[L3_TotPoint] [int] NULL,
			[L3_Cnt] [int] NULL,
			[L3_TotCnt] [int] NULL,
			[L4_Point] [int] NULL,
			[L4_Point_Lev] [int] NULL,
			[L4_Point_Day] [datetime] NULL,
			[L4_Ave] [int] NULL,
			[L4_Ave_Lev] [int] NULL,
			[L4_Ave_Day] [datetime] NULL,
			[L4_KillCnt] [int] NULL,
			[L4_KillCnt_Lev] [int] NULL,
			[L4_KillCnt_Day] [datetime] NULL,
			[L4_Contri] [int] NULL,
			[L4_Contri_Lev] [int] NULL,
			[L4_Contri_Day] [datetime] NULL,
			[L4_TotPoint] [int] NULL,
			[L4_Cnt] [int] NULL,
			[L4_TotCnt] [int] NULL,
			[L5_Point] [int] NULL,
			[L5_Point_Lev] [int] NULL,
			[L5_Point_Day] [datetime] NULL,
			[L5_Ave] [int] NULL,
			[L5_Ave_Lev] [int] NULL,
			[L5_Ave_Day] [datetime] NULL,
			[L5_KillCnt] [int] NULL,
			[L5_KillCnt_Lev] [int] NULL,
			[L5_KillCnt_Day] [datetime] NULL,
			[L5_Contri] [int] NULL,
			[L5_Contri_Lev] [int] NULL,
			[L5_Contri_Day] [datetime] NULL,
			[L5_TotPoint] [int] NULL,
			[L5_Cnt] [int] NULL,
			[L5_TotCnt] [int] NULL,
			[L6_Point] [int] NULL,
			[L6_Point_Lev] [int] NULL,
			[L6_Point_Day] [datetime] NULL,
			[L6_Ave] [int] NULL,
			[L6_Ave_Lev] [int] NULL,
			[L6_Ave_Day] [datetime] NULL,
			[L6_KillCnt] [int] NULL,
			[L6_KillCnt_Lev] [int] NULL,
			[L6_KillCnt_Day] [datetime] NULL,
			[L6_Contri] [int] NULL,
			[L6_Contri_Lev] [int] NULL,
			[L6_Contri_Day] [datetime] NULL,
			[L6_TotPoint] [int] NULL,
			[L6_Cnt] [int] NULL,
			[L6_TotCnt] [int] NULL,
			[L7_Point] [int] NULL,
			[L7_Point_Lev] [int] NULL,
			[L7_Point_Day] [datetime] NULL,
			[L7_Ave] [int] NULL,
			[L7_Ave_Lev] [int] NULL,
			[L7_Ave_Day] [datetime] NULL,
			[L7_KillCnt] [int] NULL,
			[L7_KillCnt_Lev] [int] NULL,
			[L7_KillCnt_Day] [datetime] NULL,
			[L7_Contri] [int] NULL,
			[L7_Contri_Lev] [int] NULL,
			[L7_Contri_Day] [datetime] NULL,
			[L7_TotPoint] [int] NULL,
			[L7_Cnt] [int] NULL,
			[L7_TotCnt] [int] NULL,
			[L8_Point] [int] NULL,
			[L8_Point_Lev] [int] NULL,
			[L8_Point_Day] [datetime] NULL,
			[L8_Ave] [int] NULL,
			[L8_Ave_Lev] [int] NULL,
			[L8_Ave_Day] [datetime] NULL,
			[L8_KillCnt] [int] NULL,
			[L8_KillCnt_Lev] [int] NULL,
			[L8_KillCnt_Day] [datetime] NULL,
			[L8_Contri] [int] NULL,
			[L8_Contri_Lev] [int] NULL,
			[L8_Contri_Day] [datetime] NULL,
			[L8_TotPoint] [int] NULL,
			[L8_Cnt] [int] NULL,
			[L8_TotCnt] [int] NULL,
			[L9_Point] [int] NULL,
			[L9_Point_Lev] [int] NULL,
			[L9_Point_Day] [datetime] NULL,
			[L9_Ave] [int] NULL,
			[L9_Ave_Lev] [int] NULL,
			[L9_Ave_Day] [datetime] NULL,
			[L9_KillCnt] [int] NULL,
			[L9_KillCnt_Lev] [int] NULL,
			[L9_KillCnt_Day] [datetime] NULL,
			[L9_Contri] [int] NULL,
			[L9_Contri_Lev] [int] NULL,
			[L9_Contri_Day] [datetime] NULL,
			[L9_TotPoint] [int] NULL,
			[L9_Cnt] [int] NULL,
			[L9_TotCnt] [int] NULL
		) ON [PRIMARY]
		END
		
		IF NOT EXISTS (SELECT * FROM soddb.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE ''sod2clan'+@yymm+''')
		BEGIN
		CREATE TABLE [SoDDB].[dbo].[sod2clan'+@yymm+'](
			[ChName] [varchar](50) NULL,
			[Point] [int] NULL,
			[RegistDay] [datetime] NULL,
			[ClanName] [varchar](50) NOT NULL
		) ON [PRIMARY]
		END
		
		IF NOT EXISTS (SELECT * FROM soddb.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE ''sod2record'+@yymm+''')
		BEGIN
		CREATE TABLE [SoDDB].[dbo].[sod2record'+@yymm+'](
			[SNo] [int] NOT NULL,
			[UserID] [varchar](50) NULL,
			[CharName] [varchar](50) NULL,
			[CharType] [int] NULL,
			[Point] [int] NULL,
			[KillCount] [int] NULL,
			[GLevel] [int] NULL,
			[TotalPoint] [int] NULL,
			[TotalUser] [int] NULL,
			[SuccessUser] [int] NULL,
			[ServerName] [varchar](50) NULL,
			[PCRNo] [int] NULL,
			[GPCode] [varchar](50) NULL,
			[Contribute] [int] NULL,
			[RegistDay] [datetime] NULL,
			[BusinessName] [varchar](100) NULL,
			[BAddress1] [varchar](100) NULL,
			[PMNo] [int] NULL,
			[ServerTime] [int] NULL
		) ON [PRIMARY]
		END
		
		IF NOT EXISTS (SELECT * FROM soddb.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE ''sodclan'+@yymm+''')
		BEGIN
		CREATE TABLE [SoDDB].[dbo].[sodclan'+@yymm+'](
			[IDX] [int] NOT NULL,
			[CIDX] [int] NOT NULL,
			[CServerTime] [float] NOT NULL,
			[Server] [int] NOT NULL,
			[ClanName] [varchar](20) NOT NULL,
			[ServerName] [varchar](50) NOT NULL,
			[CPoint] [int] NOT NULL,
			[CRegistDay] [datetime] NULL
		) ON [PRIMARY]
		END
		
		IF NOT EXISTS (SELECT * FROM soddb.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE ''sodclanlog'+@yymm+''')
		BEGIN
		CREATE TABLE [SoDDB].[dbo].[sodclanlog'+@yymm+'](
			[IDX] [int] NOT NULL,
			[LCIDX] [int] NOT NULL,
			[LCServerTime] [float] NOT NULL,
			[LServer] [int] NOT NULL,
			[LCPoint] [int] NOT NULL,
			[LCRegistDay] [datetime] NULL
		) ON [PRIMARY]
		END
		
		IF NOT EXISTS (SELECT * FROM soddb.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE ''sodclansum'+@yymm+''')
		BEGIN
		CREATE TABLE [SoDDB].[dbo].[sodclansum'+@yymm+'](
			[IDX] [int] NOT NULL,
			[CIDX] [int] NOT NULL,
			[Server] [int] NOT NULL,
			[ClanName] [varchar](20) NOT NULL,
			[ServerName] [varchar](50) NOT NULL,
			[CPointSum] [int] NOT NULL,
			[CRegistDay] [datetime] NULL
		) ON [PRIMARY]
		END
		
		IF NOT EXISTS (SELECT * FROM soddb.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE ''sodrecord'+@yymm+''')
		BEGIN
		CREATE TABLE [SoDDB].[dbo].[sodrecord'+@yymm+'](
			[SNo] [int] NOT NULL,
			[UserID] [varchar](50) NULL,
			[CharName] [varchar](50) NULL,
			[CharType] [int] NULL,
			[Point] [int] NULL,
			[KillCount] [int] NULL,
			[GLevel] [int] NULL,
			[TotalPoint] [int] NULL,
			[TotalUser] [int] NULL,
			[SuccessUser] [int] NULL,
			[ServerName] [varchar](50) NULL,
			[PCRNo] [int] NULL,
			[GPCode] [varchar](50) NULL,
			[BusinessName] [varchar](100) NULL,
			[BAddress1] [varchar](100) NULL,
			[PMNo] [int] NULL,
			[Contribute] [int] NULL,
			[RegistDay] [datetime] NULL,
			[ServerTime] [float] NULL,
			[Flag] [int] NULL,
			[ClanIDX] [int] NULL,
			[ClanName] [varchar](50) NULL
		) ON [PRIMARY]
		END
		
		IF NOT EXISTS (SELECT * FROM soddb.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE ''sofclanlog'+@yymm+''')
		BEGIN
		CREATE TABLE [SoDDB].[dbo].[sofclanlog'+@yymm+'](
			[IDX] [int] NOT NULL,
			[LCIDX] [int] NOT NULL,
			[LCServerTime] [float] NOT NULL,
			[LServer] [int] NOT NULL,
			[LCPoint] [int] NOT NULL,
			[LCRegistDay] [datetime] NULL
		) ON [PRIMARY]
		END

		SET ANSI_PADDING OFF
	');
END