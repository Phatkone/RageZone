PristonTale Database SQL Scripts
Created by Phatkone AKA EuphoriA AKA Ashikabi

Purpose:
	To create the databases required to run a standard PristonTale server.

Creates:
	Service account: ptsql - password: PristontaleSQLAcc3ss
	AccountDB and all required subtables
	ClanDB and all required subtables
	SoDDB and all subtables
		Levellist table for server ranking (See my levellist script)
	makeSodMonthlyTables procedure:
		run on the first of every month to create the monthly tables.
	Block procedure:
		Procedure for checking block status of an account.
	Is_Blocked Function:
		Function for checking block status of an account. (easier to call within a script than the procedure)
	includes: 'Block_Check_Tables.sql'
		Returns the accounts that are blocked, along with the table name.
Use:

Execute 'Create_AccountDB.sql' first as it creates the user login in the first task.
'Block_Check_Tables' AND 'Block_Procedure.sql' CANNOT BE EXECUTED BEFORE 'Create_AccountDB.sql'.
'Create_SoDDB_Monthly.sql' must be executed AFTER 'Create_SoDDB.sql'.

Credits:
	All scripts are created by and credited to the owner (Me, EuphoriA).
	Users are free to manipulate and use scripts at their discretion.

