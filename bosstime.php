<?php
require_once 'dbconn.php'; 
date_default_timezone_set('America/Los_Angeles'); // set to server time
$logfile = "D:/PT-Server/LogFile/";  // set path to LogFile folder
$db = "GameDB";			// Database
$table = "BossTime";	// Table 
/**
*	No more changes to make.
*/
$logfile .= (substr(logfile,-1) == "/") ? "(BossMonster)$md.log" : "/(BossMonster)$md.log";
$md = date('n-j');
function searchlog ($file)
{
	$bossTime = false;
	$regex = '&(?<hour>(\d{1,2}))\:(?<minute>(\d{1,2})).+&';
	$text = file($file);
	if (preg_match($regex, end($text), $matches)) 
	{
		$bossTime = "XX:".$matches['minute'];
	}
	return $bossTime;
}
while (!is_file($logfile))
{
	sleep(30);
}
$bossTime = searchlog($logfile);
if ($bossTime !== false && $dbconn)
{
	$query = "IF (SELECT COUNT(*) AS Rows FROM [{$db}].[dbo].[{$table}]) <= 0
	INSERT INTO [{$db}].[dbo].[{$table}] SET (Bosstime) VALUES ('$bossTime')
ELSE
	UPDATE [{$db}].[dbo].[{$table}] SET Bosstime='$bossTime'";
	sqlsrv_query($dbconn, $query);
	sqlsrv_close($dbconn);
}
?>