<?php

$dir = "D:/PT-Server/";
$logfile = "D:/logs/postbox.log";
date_default_timezone_set('America/Los_Angeles'); //Server location for correct log timestamping

/*No changes needed below here */
header('Access-Control-Allow-Origin: *');
function numDir($name)
{
	$total = 0;
	for($i=0;$i<strlen($name);$i++) {
		$total+=ord(strtoupper($name[$i]));
		if($total>=256) {
			$total=$total-256;
		}
	}
	return $total;
}

$user = (isset($_REQUEST['user'])) ? $_REQUEST['user'] : false;
$chName = (isset($_REQUEST['chName'])) ? $_REQUEST['chName'] : false;
$itCode = (isset($_REQUEST['itCode'])) ? $_REQUEST['itCode'] : false;
$spec = (isset($_REQUEST['spec'])) ? $_REQUEST['spec'] : false;
$msg = (isset($_REQUEST['msg'])) ? $_REQUEST['msg'] : false;
$val = (isset($_REQUEST['val'])) ? $_REQUEST['val'] : false;
if ($val && ($itCode == "gg101" || $itCode == "gg102"))
{
	$spec = $val;
}
if (!$user || !$chName || !$itCode || !$spec || !$msg)
{
	$return = "Please Fill All Fields!";
}
else
{
	if (!is_numeric($spec))
	{
		$return = "Invalid Spec Code";
	}
	else
	{
		if ($chName != "***")
		{
			$ChFile = $dir."dataserver/userdata/".numDir($chName)."/$chName.dat";
			if (is_file($ChFile))
			{
				$fopen = fopen($ChFile, 'r');
				$fread = fread($fopen, filesize($ChFile));
				$account = trim(substr($fread, 0x2D0, 16),"\x00");
				fclose($fopen);
				if (strtolower($user) != strtolower($account))
				{
					$return = "Character and Account Mismatch Error!";
				}
			}
			else
			{
				$return = "Character File Not Found!";
				die(json_encode($return));
			}
		}
		$postbox = $dir."/PostBox/".numDir($user)."/$user.dat";
		$string = "$chName		$itCode		$spec		\"$msg\"";
		$write = file_put_contents($postbox, $string.PHP_EOL,FILE_APPEND);
		if ($write)
		{
			$itCode = ($itCode == 'gg101') ? "$val Gold" : $itCode;
			$itCode = ($itCode == 'gg102') ? "$val EXP" : $itCode;
			$chName = ($chName == "***") ? "Any" : $chName;
			$return = "Successfully posted $itCode to $user : $chName";
			file_put_contents($logfile, date('Y-m-d').": $user -> $itCode".PHP_EOL, FILE_APPEND);
		}
		else
		{
			$return = "Something went wrong... *Shrugs and walks away*";
			file_put_contents($logfile, date('Y-m-d').": $user -> $itCode   -- Error".PHP_EOL, FILE_APPEND);
		}
	}
}
print(json_encode($return));
?>