﻿$dir = "D:\PT-Server\LogItemData\"

$date = Get-Date
$lastMonth = $date.AddMonths(-1)
$files = Get-ChildItem -LiteralPath $dir | Where-Object {$_.LastWriteTime -lt $lastMonth} | foreach {
    Remove-item $_.FullName
}